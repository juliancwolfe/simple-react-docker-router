FROM node:9-alpine

# Build production client
WORKDIR /usr/tmp/app
COPY ./package*.json ./
RUN npm install
COPY ./ .
RUN npm run build && \
    mkdir -p /usr/src/app/build && \
    cp -r /usr/tmp/app/build/* /usr/src/app/build && \
    rm -rf /usr/tmp/app

# Build static server
WORKDIR /usr/src/app
COPY ./server/package*.json ./
RUN npm install
COPY ./server .

EXPOSE 8080

CMD ["npm", "start"]