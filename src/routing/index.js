import history from './history';
import {makeRoutes} from './routes';

export {
    history,
    makeRoutes
};