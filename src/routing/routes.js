import React from 'react';
import {Route, Router, Switch} from 'react-router-dom';
import App from '../App';
import history from './history';
import {NotFoundController, MainController} from '../controllers';

export const makeRoutes = () => {
    return (
        <Router history={history} component={App}>
            <Switch>
                <Route exact path="/" render={(props) => <App {...props}><MainController/></App>}/>
                <Route exact path="*" render={(props) => <App {...props}><NotFoundController {...props}/></App>}/>
            </Switch>
        </Router>
    );
}