import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {makeRoutes} from './routing';

const routes = makeRoutes();

ReactDOM.render(routes, document.getElementById('root'));
registerServiceWorker();
