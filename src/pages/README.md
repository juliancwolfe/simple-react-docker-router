Each page is in its own folder in order to group styles without making imports difficult throughout the app.

In React, you import styles the same way you import JS files, but you specify the CSS extension.

Example:

Say you have a NotFound page and you have some CSS for it. That CSS for that page will be stored in a NotFound.css file in the NotFound folder alongside the NotFound.js file which has the JSX markup and component interaction logic.

In the NotFound.js file, your import will look like this:

```
import './NotFound.css`
//other stuff

export default class NotFound extends Component {
    //React stuff
}
```

The following index.js file in the same folder will make it such that whenever you specify the folder in an import, it returns the corresponding module.

```
import NotFound from './NotFound';
export default NotFound;
```

> The import statement will recognize javascript files by default so no need to specify the extension there.

Because we added this index.js file, we are now able to import the folder and retrieve the NotFound component instead of having to import the entire directory. This makes it easier to structure the app and reduces the possible nesting which can become confusing with deeper folder structures.

Effectively, relative to the folder position when being imported, the redundant path gets replaced.

```
//without any index.js at all
import NotFound from './pages/NotFound/NotFound';
//without index.js in the NotFound folder
import {NotFound} from './pages/NotFound/NotFound';
//with index.js in the NotFound folder
import {NotFound} from './pages/NotFound';
```