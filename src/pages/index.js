import MainPage from './MainPage';
import NotFound from './NotFound';

export {
    NotFound,
    MainPage
};