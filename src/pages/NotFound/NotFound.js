import React, {Component} from 'react';

export default class NotFound extends Component {
    render() {
        return (
            <div>
                <h2>Page not found, sorry!</h2>
            </div>
        );
    }
}