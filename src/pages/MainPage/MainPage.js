import React, {Component} from 'react';

export default class MainPage extends Component {
    render() {
        return (
            <div>
                <h2>Welcome to the main page</h2>
            </div>
        );
    }
}