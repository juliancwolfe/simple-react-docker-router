Controllers are React components in this case but the key difference between a Controller and a Page is the type of logic that is handled.

To separate interaction logic from view logic, the page markup is placed in a page in the pages directory, and any logic being communicated with the API is handled in the controller. This makes pages reusable and decouples presentation logic from client-side business logic.

The `Controller` class is abstract intentionally to reduce boilerplate code and act as a wrapper to expose common elements that are used with a controller like history and withRouter to reduce the places where relative paths can break.

The `withRouter` decorator needs to be wrapped around a child controller when said controller makes redirects to other pages within the application. It injects router-specific logic into the component's properties for use.