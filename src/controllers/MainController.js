import Controller, {React} from './Controller';
import {MainPage} from '../pages';

export default class MainController extends Controller {
    render() {
        return (
            <MainPage/>
        );
    }
}