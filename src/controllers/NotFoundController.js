import Controller, {React} from './Controller';
import {NotFound} from '../pages';

export default class NotFoundController extends Controller {
    render() {
        return (
            <NotFound/>
        );
    }
}