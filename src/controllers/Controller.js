import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import history from '../routing/history';

export default class Controller extends Component {}
export {
    withRouter,
    history,
    React
};