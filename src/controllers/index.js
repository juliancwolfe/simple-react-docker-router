import MainController from './MainController';
import NotFoundController from './NotFoundController';

export {
    NotFoundController,
    MainController
};